FROM openjdk:8-jre-alpine

ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS \
    JAVA_OPTS="-server \
               -Xms200m -Xmx500m \
               -XX:+UseG1GC" \
    TZ=Europe/Moscow

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

EXPOSE 8090

ADD target/*.jar /app.jar

CMD java ${JAVA_OPTS} \
        -Djava.security.egd=file:/dev/./urandom \
        -jar /app.jar