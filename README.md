# Country Phone Code 

Dockered java server with API for country phone code

##Build .jar

In cmd in root of project use following command 

    ./mvnw pacakge

That was build .jar file in  '/target' directory

## Docker Image & Container

Use next command for create image 

    docker build -t {name_of_image} .

Then create and run container 

    docker run -d -p8090:8090 --name {container_name} {name_of_image}
 
 ##API
 
 ###Get All Country
 [http://localhost:8090/api/country/](http://localhost:8090/api/country/)
 ###Get All Code
 [http://localhost:8090/api/code/](http://localhost:8090/api/code/)
 ###Search Country Code By Query
 [http://localhost:8090/api/country-code/_search?query=](http://localhost:8090/api/country-code/_search?query=)
  
