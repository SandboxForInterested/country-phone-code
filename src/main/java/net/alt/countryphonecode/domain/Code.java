package net.alt.countryphonecode.domain;

import java.io.Serializable;

public class Code implements Serializable {

    private String countryCode;

    private String phoneCode;

    public Code(){}

    public Code(String countryCode, String phoneCode){
        this.countryCode = countryCode;
        this.phoneCode = phoneCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Code && ((Code) obj).getCountryCode().equals(getCountryCode());
    }

    @Override
    public int hashCode() {
        return getCountryCode().hashCode();
    }

    @Override
    public String toString() {
        return "Code{" +
                "countryCode='" + countryCode + '\'' +
                ", phoneCode='" + phoneCode + '\''+
                '}';
    }
}
