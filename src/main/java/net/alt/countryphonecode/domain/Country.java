package net.alt.countryphonecode.domain;

public class Country {

    private String code;

    private String name;

    public Country(){}

    public Country(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Country && ((Country) obj).getCode().equals(getCode());
    }

    @Override
    public int hashCode() {
        return getCode().hashCode();
    }

    @Override
    public String toString() {
        return "Code{" +
                "code='" + code + '\'' +
                ", name='" + name + '\''+
                '}';
    }
}
