package net.alt.countryphonecode.domain;

import java.util.Objects;

public class CountryCode {

    private String code;
    private String country;
    private String name;

    public CountryCode(){}

    public CountryCode(Country country, Code code){
        this.code = code.getPhoneCode();
        this.country = country.getName();
        this.name = code.getCountryCode();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryCode that = (CountryCode) o;
        return Objects.equals(code, that.code) &&
                Objects.equals(country, that.country) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, country, name);
    }
}
