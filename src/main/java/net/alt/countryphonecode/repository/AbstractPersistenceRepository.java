package net.alt.countryphonecode.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import net.alt.countryphonecode.service.externalapi.AbstractExternalApi;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

public abstract class AbstractPersistenceRepository<T> {

    protected Logger log;
    protected Class<T> clazz;

    private Collection<T> cache;
    private long lastRefresh;
    private final long REFRESH_PERIOD = 1000 * 60 * 60 * 2;

    private final AbstractExternalApi<T> apiService;

    public AbstractPersistenceRepository(AbstractExternalApi<T> apiService){
        this.apiService = apiService;
        this.cache = new HashSet<>();
    }

    public void init(){
        log.info("Prepare repository for {} entity",clazz.getSimpleName());
        if(cache.isEmpty())
            try{
                cache.addAll(readFromFileCache());
                log.info("Correctly read from {} file",getFilename());
            } catch (Exception ex){
                log.error("Exception in {} repository : {}",clazz.getSimpleName(),ex.getLocalizedMessage());
                refresh();
            }
    }

    public synchronized void refresh(){
        if(lastRefresh+ REFRESH_PERIOD > System.currentTimeMillis())
            return;
        try {
            log.info("Update cache for entity {}",clazz.getSimpleName());
            Collection<T> resultOfRequest = apiService.getRequestForObject();
            if(!resultOfRequest.isEmpty()) {
                cache = Collections.unmodifiableCollection(resultOfRequest);
                saveToFileCache();
                log.info("Correctly update cache of {} entity", clazz.getSimpleName());
            }
        }catch (Exception ex){
            log.error("Exception in {} repository : {}",clazz.getSimpleName(),ex.getLocalizedMessage());
            log.error("Still work with cache");
        }finally {
            lastRefresh = System.currentTimeMillis();
        }
    }

    public Collection<T> getObjects(){
        if(lastRefresh + REFRESH_PERIOD <System.currentTimeMillis())
            refresh();
        return Collections.unmodifiableCollection(cache);
    }

    public void saveToFileCache() throws IOException {
        ObjectMapper mapper = getObjectMapper();
        mapper.writeValue(new File(getFilename()), cache);
        log.info("Add {} entities by count {}",clazz.getSimpleName(),cache.size());
    }

    public Collection<T> readFromFileCache() throws IOException {
        ObjectMapper mapper = getObjectMapper();
        return mapper.readValue(new File(getFilename()), getType());
    }

    abstract String getFilename();

    abstract TypeReference<Collection<T>> getType();

    private ObjectMapper getObjectMapper(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        return mapper;
    }

}
