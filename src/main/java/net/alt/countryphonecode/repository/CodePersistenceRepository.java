package net.alt.countryphonecode.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import net.alt.countryphonecode.domain.Code;
import net.alt.countryphonecode.service.externalapi.AbstractExternalApi;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CodePersistenceRepository extends AbstractPersistenceRepository<Code> {

    private static final String FILENAME = "code.json";

    {
        log= LoggerFactory.getLogger(CodePersistenceRepository.class);
        clazz = Code.class;
    }

    public CodePersistenceRepository(AbstractExternalApi<Code> apiService) {
        super(apiService);
    }

    @Override
    String getFilename() {
        return FILENAME;
    }

    @Override
    TypeReference<Collection<Code>> getType() {
        return new TypeReference<Collection<Code>>() {};
    }
}
