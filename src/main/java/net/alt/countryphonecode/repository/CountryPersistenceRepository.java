package net.alt.countryphonecode.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import net.alt.countryphonecode.domain.Country;
import net.alt.countryphonecode.service.externalapi.AbstractExternalApi;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.Collection;

@Service
public class CountryPersistenceRepository extends AbstractPersistenceRepository<Country> {

    private static final String FILENAME = "country.json";

    {
        log= LoggerFactory.getLogger(CountryPersistenceRepository.class);
        clazz = Country.class;
    }

    public CountryPersistenceRepository(AbstractExternalApi<Country> apiService) {
        super(apiService);
    }

    @Override
    String getFilename() {
        return FILENAME;
    }

    @Override
    TypeReference<Collection<Country>> getType() {
        return new TypeReference<Collection<Country>>() {};
    }
}
