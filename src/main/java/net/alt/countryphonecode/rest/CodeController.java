package net.alt.countryphonecode.rest;

import net.alt.countryphonecode.domain.Code;
import net.alt.countryphonecode.service.CodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/api/code")
public class CodeController {

    private final CodeService service;
    protected Logger log = LoggerFactory.getLogger(CodeController.class);

    public CodeController(CodeService service){
        this.service = service;
    }


    @GetMapping("/")
    public Collection<Code> getAllCodes(){
        return service.getCode();
    }
}
