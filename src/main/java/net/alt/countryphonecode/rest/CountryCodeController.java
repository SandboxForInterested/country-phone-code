package net.alt.countryphonecode.rest;

import net.alt.countryphonecode.domain.CountryCode;
import net.alt.countryphonecode.service.CountryCodeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/api/country-code")
public class CountryCodeController {

    private final CountryCodeService service;

    public CountryCodeController(CountryCodeService service){
        this.service = service;
    }

    @GetMapping("/_search")
    public Collection<CountryCode> searchCountryCode(@RequestParam String query){
        return service.search(query);
    }

}
