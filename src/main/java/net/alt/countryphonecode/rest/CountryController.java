package net.alt.countryphonecode.rest;

import net.alt.countryphonecode.domain.Country;
import net.alt.countryphonecode.service.CountryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/api/country")
public class CountryController {

    private final CountryService service;
    protected Logger log = LoggerFactory.getLogger(CodeController.class);

    public CountryController(CountryService service){
        this.service = service;
    }


    @GetMapping("/")
    public Collection<Country> getAllCountry(){
        return service.getCountry();
    }
}
