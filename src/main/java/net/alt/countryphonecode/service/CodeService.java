package net.alt.countryphonecode.service;

import net.alt.countryphonecode.domain.Code;
import net.alt.countryphonecode.repository.AbstractPersistenceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class CodeService {

    protected Logger log = LoggerFactory.getLogger(CodeService.class);

    private final AbstractPersistenceRepository<Code> repository;
    private Map<String,Code> serviceCache;

    public CodeService(AbstractPersistenceRepository<Code> repository){
        this.repository = repository;
        repository.init();
        serviceCache = repository.getObjects().stream().collect(Collectors.toMap(Code::getCountryCode, Function.identity()));
    }

    public Collection<Code> getCode(){
        Collection<Code> result;
        try {
            result = repository.getObjects();
            log.info("Get prepared 'code' objects");
        }catch (Exception ex){
            log.error("Exception in CodesService: {}",ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
        return result;
    }

    public Optional<Code> getCodeByCountryCode(String code){
        return Optional.ofNullable(serviceCache.get(code));
    }
}
