package net.alt.countryphonecode.service;

import net.alt.countryphonecode.domain.Code;
import net.alt.countryphonecode.domain.CountryCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CountryCodeService {

    protected Logger log = LoggerFactory.getLogger(CountryCodeService.class);

    private final CodeService codeService;
    private final CountryService countryService;

    public CountryCodeService(CodeService codeService, CountryService countryService){
        this.codeService = codeService;
        this.countryService = countryService;
    }

    public Collection<CountryCode> search(String query){
        Collection<CountryCode> result = new HashSet<>();
        if(!query.isEmpty()){
            result.addAll(countryService.getCountry().stream().filter(country -> country.getName().contains(query))
                    .map(country -> {
                        Optional<Code> code = codeService.getCodeByCountryCode(country.getCode());
                        if(code.isPresent())
                            return new CountryCode(country, code.get());
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet()));
            log.info("Found {} rows for query string {}",result.size(),query);
        }else
            log.info("Request string is empty");
        return result;
    }
}
