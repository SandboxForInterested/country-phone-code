package net.alt.countryphonecode.service;

import net.alt.countryphonecode.domain.Country;
import net.alt.countryphonecode.repository.AbstractPersistenceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CountryService {

    private final AbstractPersistenceRepository<Country> repository;
    protected Logger log = LoggerFactory.getLogger(CountryService.class);

    public CountryService(AbstractPersistenceRepository<Country> repository){
        this.repository = repository;
        repository.init();
    }

    public Collection<Country> getCountry(){
        Collection<Country> result;
        try {
            result = repository.getObjects();
            log.info("Get prepared 'country' objects");
        }catch (Exception ex){
            log.error("Exception in CountryService: {}",ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        }
        return result;
    }
}
