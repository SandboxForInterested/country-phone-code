package net.alt.countryphonecode.service.externalapi;

import org.slf4j.Logger;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collection;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public abstract class AbstractExternalApi<T> {

    protected Logger log;
    protected Class<T> clazz;

    public abstract String getUrl();

    public abstract T createObjectFromStringArray(String[] array);

    public Collection<T> getRequestForObject(){
        String countries = requestForExternalApi();
        return convertToObject(countries);
    }

    private String requestForExternalApi(){
        return new RestTemplate().getForObject(getUrl(), String.class);
    }

    private Collection<T> convertToObject(String source){
        log.info("Convert {} entity",clazz.getSimpleName());
        return Arrays.stream(source.replaceAll("[{}]","").split(Pattern.quote("\","))).map(s -> {
            String[] codeAndName = s.replaceAll(Pattern.quote("\""),"").split(Pattern.quote(":"));
            return createObjectFromStringArray(codeAndName);
        }).collect(Collectors.toList());
    }
}
