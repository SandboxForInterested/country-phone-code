package net.alt.countryphonecode.service.externalapi;

import net.alt.countryphonecode.domain.Code;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ExternalCodeApiService extends AbstractExternalApi<Code> {

    private final static String CODE_URL = "http://country.io/phone.json";

    {
        log= LoggerFactory.getLogger(ExternalCodeApiService.class);
        clazz = Code.class;
    }

    @Override
    public String getUrl() {
        return CODE_URL;
    }

    @Override
    public Code createObjectFromStringArray(String[] array) {
        if(array == null || array.length != 2)
            throw new IllegalArgumentException("Not valid array");
        return new Code(array[0],array[1]);
    }
}
