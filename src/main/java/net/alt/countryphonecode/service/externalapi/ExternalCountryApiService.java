package net.alt.countryphonecode.service.externalapi;

import net.alt.countryphonecode.domain.Country;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ExternalCountryApiService extends AbstractExternalApi<Country> {
    private final static String COUNTRY_URL = "http://country.io/names.json";

    {
        log= LoggerFactory.getLogger(ExternalCountryApiService.class);
        clazz = Country.class;
    }

    @Override
    public String getUrl() {
        return COUNTRY_URL;
    }

    @Override
    public Country createObjectFromStringArray(String[] array) {
        if(array == null || array.length != 2)
            throw new IllegalArgumentException("Not valid array");
        return new Country(array[0],array[1]);
    }
}
