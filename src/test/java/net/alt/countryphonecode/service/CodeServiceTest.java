package net.alt.countryphonecode.service;

import net.alt.countryphonecode.domain.Code;
import net.alt.countryphonecode.repository.AbstractPersistenceRepository;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class CodeServiceTest {

    @InjectMocks
    CodeService service;

    @Mock
    AbstractPersistenceRepository<Code> repository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getCode() {
        List<Code> list = new ArrayList<>();
        list.add(new Code("test1","test1"));
        list.add(new Code("test2","test2"));

        when(repository.getObjects()).thenReturn(list);

        Collection<Code> result = service.getCode();

        assertEquals(2,result.size());
        assertEquals(list,result);
    }
}