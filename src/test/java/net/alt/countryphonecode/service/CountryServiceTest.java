package net.alt.countryphonecode.service;

import net.alt.countryphonecode.domain.Code;
import net.alt.countryphonecode.domain.Country;
import net.alt.countryphonecode.repository.AbstractPersistenceRepository;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
class CountryServiceTest {

    @InjectMocks
    CountryService service;

    @Mock
    AbstractPersistenceRepository<Country> repository;


    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getCountry() {

        List<Country> list = new ArrayList<>();
        list.add(new Country("test1","test1"));
        list.add(new Country("test2","test2"));

        when(repository.getObjects()).thenReturn(list);

        Collection<Country> result = service.getCountry();

        assertEquals(2,result.size());
        assertEquals(list,result);
    }
}